<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;

class Coords extends Model
{
    use PostgisTrait;

    public $timestamps = false;

    protected $fillable = [
        'coords_user_id',
        'coords_company_id',
        'coords_clock'
    ];

    protected $postgisFields = [
        'coords_point',
    ];

    protected $postgisTypes = [
        'coords_point' => [
            'geomtype' => 'geography',
            'srid' => 27700
        ]
    ];
}
