<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Coords;
use DateTime;
use DateTimeZone;
use MStaack\LaravelPostgis\Geometries\Point;

class CoordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function coords(Request $request)
    {
        $data = $request->get('data');
        $id = $request->get('user');
        $company = $request->get('company');
        $response = [
            'status' => false
        ];

        if ((!$data || !is_array($data) || !sizeof($data)) || (!$id || !is_int($id)) || (!$company || !is_int($company))) {

            if (!$id || !is_int($id))
                $response['error'][] = 'not defined user or wrong format';

            if (!$company || !is_int($company))
                $response['error'][] = 'not defined company or wrong format';

            if (!$data || !is_array($data) || !sizeof($data))
                $response['error'][] = 'not defined array or wrong format.';

            return response()->json($response, 422);
        }

        foreach ($data as $point) {

            $lat = isset($point['lat']) ? $point['lat'] : null;
            $lng = isset($point['lng']) ? $point['lng'] : null;
            $clock = isset($point['date']) ? DateTime::createFromFormat('Y-m-d H:i:s', $point['date']) : new DateTime('now', new DateTimeZone('GMT'));

            if ((!$lat && !is_float($lat)) || (!$lng && !is_float($lng)) || (!$clock)) {

                if (!$lat)
                    $response['error'][] = 'not defined latitude or wrong format';

                if (!$lng)
                    $response['error'][] = 'not defined longitude or wrong format';

                if (!$clock)
                    $response['error'][] = 'not defined date or wrong format';

                return response()->json($response, 422);
            }

            // store
            $coords = new Coords;
            $coords->coords_point = new Point($lat, $lng);
            $coords->coords_user_id = $id;
            $coords->coords_company_id = $company;
            $coords->coords_clock = $clock->format('U');
            $coords->save();
        }

        // response
        $response['status'] = true;
        $response['msg'] = 'Saved correctly!';

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
