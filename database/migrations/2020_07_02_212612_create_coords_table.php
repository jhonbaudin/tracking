<?php

use Illuminate\Database\Migrations\Migration;

class CreateCoordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE TABLE public.coords (
                id SERIAL NOT NULL,
                coords_point geography(POINT) NOT NULL,
                coords_user_id int4 NOT NULL,
                coords_company_id int4 NOT NULL,
                coords_clock float8 NOT NULL DEFAULT date_part('epoch'::text, now())
            )
            PARTITION BY RANGE (coords_clock);
            CREATE INDEX coords_clock_idx ON ONLY public.coords USING btree (coords_clock);
            CREATE INDEX coords_company_id_idx ON ONLY public.coords USING btree (coords_company_id);
            CREATE INDEX coords_user_id_idx ON ONLY public.coords USING btree (coords_user_id);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TABLE public.coords");
    }
}
