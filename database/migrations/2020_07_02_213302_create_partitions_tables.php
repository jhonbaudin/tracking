<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartitionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = new DateTime('now', new DateTimeZone('GMT'));
        $ymd = DateTime::createFromFormat('Y-m-d H:i:s',  $now->format('Y-m-d') . ' 00:00:00');
        $time = (int) $ymd->format('U');
        $year = $time + 30931200;
        for ($i = $time; $i <= $year; $i += 86400) {
            $next = $i + 86399;
            $date = DateTime::createFromFormat('U',  $i);
            DB::unprepared('CREATE TABLE p_coords_' . $date->format('Y_m_d') . ' PARTITION OF coords FOR VALUES FROM (' . $i . ') TO (' . $next . ')');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partitions_tables');
    }
}
